/********************************************************************************
** Form generated from reading UI file 'extending.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXTENDING_H
#define UI_EXTENDING_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>

QT_BEGIN_NAMESPACE

class Ui_Extending
{
public:

    void setupUi(QDialog *Extending)
    {
        if (Extending->objectName().isEmpty())
            Extending->setObjectName(QString::fromUtf8("Extending"));
        Extending->resize(400, 300);

        retranslateUi(Extending);

        QMetaObject::connectSlotsByName(Extending);
    } // setupUi

    void retranslateUi(QDialog *Extending)
    {
        Extending->setWindowTitle(QCoreApplication::translate("Extending", "Extending", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Extending: public Ui_Extending {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXTENDING_H
