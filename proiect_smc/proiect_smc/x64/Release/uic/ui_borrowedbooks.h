/********************************************************************************
** Form generated from reading UI file 'borrowedbooks.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BORROWEDBOOKS_H
#define UI_BORROWEDBOOKS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BorrowedBooks
{
public:
    QWidget *widget;
    QPushButton *backBtn;
    QWidget *widget_2;
    QTableView *tableView;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *returnBtn;
    QPushButton *extendBtn;

    void setupUi(QDialog *BorrowedBooks)
    {
        if (BorrowedBooks->objectName().isEmpty())
            BorrowedBooks->setObjectName(QString::fromUtf8("BorrowedBooks"));
        BorrowedBooks->resize(880, 436);
        widget = new QWidget(BorrowedBooks);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(-10, -20, 901, 461));
        widget->setStyleSheet(QString::fromUtf8("#widget{\n"
"border-image: url(:/MainWindow/mylibrary.jpg);\n"
"  color:white;\n"
"}"));
        backBtn = new QPushButton(widget);
        backBtn->setObjectName(QString::fromUtf8("backBtn"));
        backBtn->setGeometry(QRect(714, 30, 141, 24));
        backBtn->setStyleSheet(QString::fromUtf8("#backBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#backBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));
        widget_2 = new QWidget(widget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setGeometry(QRect(40, 70, 811, 361));
        widget_2->setStyleSheet(QString::fromUtf8("#widget_2{\n"
"border-image: url(:/MainWindow/page.jpg);\n"
"  color:white;\n"
"}"));
        tableView = new QTableView(widget_2);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setGeometry(QRect(0, 0, 631, 361));
        tableView->setStyleSheet(QString::fromUtf8("#tableView{\n"
"background:transparent;\n"
"  color:rgb(88, 54, 34);\n"
"}"));
        layoutWidget = new QWidget(widget_2);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(640, 120, 161, 141));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        returnBtn = new QPushButton(layoutWidget);
        returnBtn->setObjectName(QString::fromUtf8("returnBtn"));
        returnBtn->setStyleSheet(QString::fromUtf8("#returnBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#returnBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(returnBtn);

        extendBtn = new QPushButton(layoutWidget);
        extendBtn->setObjectName(QString::fromUtf8("extendBtn"));
        extendBtn->setStyleSheet(QString::fromUtf8("#extendBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#extendBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(extendBtn);


        retranslateUi(BorrowedBooks);

        QMetaObject::connectSlotsByName(BorrowedBooks);
    } // setupUi

    void retranslateUi(QDialog *BorrowedBooks)
    {
        BorrowedBooks->setWindowTitle(QCoreApplication::translate("BorrowedBooks", "BorrowedBooks", nullptr));
        backBtn->setText(QCoreApplication::translate("BorrowedBooks", "Back to Main Page", nullptr));
        returnBtn->setText(QCoreApplication::translate("BorrowedBooks", "Return Book", nullptr));
        extendBtn->setText(QCoreApplication::translate("BorrowedBooks", "Extend Booking Time", nullptr));
    } // retranslateUi

};

namespace Ui {
    class BorrowedBooks: public Ui_BorrowedBooks {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BORROWEDBOOKS_H
