/********************************************************************************
** Form generated from reading UI file 'reader.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_READER_H
#define UI_READER_H

#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>

QT_BEGIN_NAMESPACE

class Ui_Reader
{
public:

    void setupUi(QObject *Reader)
    {
        if (Reader->objectName().isEmpty())
            Reader->setObjectName(QString::fromUtf8("Reader"));
        Reader->resize(400, 300);

        retranslateUi(Reader);

        QMetaObject::connectSlotsByName(Reader);
    } // setupUi

    void retranslateUi(QObject *Reader)
    {
        Reader->setWindowTitle(QCoreApplication::translate("Reader", "Reader", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Reader: public Ui_Reader {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_READER_H
