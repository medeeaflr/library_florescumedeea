/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QGroupBox *loginForm;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_2;
    QLabel *usernameLogin;
    QLineEdit *username_2;
    QLabel *passwordLogin;
    QLineEdit *password_2;
    QPushButton *LogInBtn;
    QGroupBox *registerForm;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_11;
    QLabel *label_2;
    QLineEdit *username;
    QLabel *label_3;
    QLineEdit *password;
    QLabel *label_4;
    QLineEdit *firstName;
    QLabel *label_5;
    QLineEdit *lastName;
    QPushButton *SignUpBtn;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QString::fromUtf8("MainWindowClass"));
        MainWindowClass->resize(1677, 1080);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/icon.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindowClass->setWindowIcon(icon);
        MainWindowClass->setStyleSheet(QString::fromUtf8(""));
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setStyleSheet(QString::fromUtf8("#centralWidget{\n"
"	\n"
"	border-image: url(:/MainWindow/mylibrary.jpg);\n"
"  color:white;\n"
"}"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(350, -500, 1920, 1080));
        QFont font;
        font.setPointSize(36);
        font.setBold(false);
        label->setFont(font);
        label->setStyleSheet(QString::fromUtf8("#label{\n"
"color:white;\n"
"\n"
"}"));
        loginForm = new QGroupBox(centralWidget);
        loginForm->setObjectName(QString::fromUtf8("loginForm"));
        loginForm->setGeometry(QRect(770, 170, 271, 311));
        QFont font1;
        font1.setPointSize(26);
        loginForm->setFont(font1);
        loginForm->setStyleSheet(QString::fromUtf8("#loginForm{\n"
"	\n"
" background: url(:/MainWindow/logbg.png) no-repeat center center fixed;\n"
"border-radius:8px;\n"
"color:white;\n"
"\n"
"}"));
        verticalLayoutWidget_3 = new QWidget(loginForm);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(10, 40, 251, 261));
        verticalLayout_5 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        usernameLogin = new QLabel(verticalLayoutWidget_3);
        usernameLogin->setObjectName(QString::fromUtf8("usernameLogin"));
        usernameLogin->setEnabled(true);
        QFont font2;
        font2.setPointSize(20);
        usernameLogin->setFont(font2);
        usernameLogin->setStyleSheet(QString::fromUtf8("#usernameLogin\n"
"{\n"
"color:white;\n"
"}\n"
""));

        verticalLayout_2->addWidget(usernameLogin);

        username_2 = new QLineEdit(verticalLayoutWidget_3);
        username_2->setObjectName(QString::fromUtf8("username_2"));
        username_2->setStyleSheet(QString::fromUtf8("#username_2\n"
"{\n"
"background-color: rgb(56, 40, 30);\n"
"color:white;\n"
"}\n"
""));

        verticalLayout_2->addWidget(username_2);

        passwordLogin = new QLabel(verticalLayoutWidget_3);
        passwordLogin->setObjectName(QString::fromUtf8("passwordLogin"));
        passwordLogin->setFont(font2);
        passwordLogin->setStyleSheet(QString::fromUtf8("#passwordLogin\n"
"{\n"
"color:white;\n"
"}\n"
""));

        verticalLayout_2->addWidget(passwordLogin);

        password_2 = new QLineEdit(verticalLayoutWidget_3);
        password_2->setObjectName(QString::fromUtf8("password_2"));
        password_2->setStyleSheet(QString::fromUtf8("#password_2\n"
"{\n"
"background-color: rgb(56, 40, 30);\n"
"color:white;\n"
"}\n"
""));
        password_2->setEchoMode(QLineEdit::Password);

        verticalLayout_2->addWidget(password_2);


        verticalLayout_5->addLayout(verticalLayout_2);

        LogInBtn = new QPushButton(verticalLayoutWidget_3);
        LogInBtn->setObjectName(QString::fromUtf8("LogInBtn"));
        LogInBtn->setFont(font2);
        LogInBtn->setStyleSheet(QString::fromUtf8("#LogInBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#LogInBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout_5->addWidget(LogInBtn);

        registerForm = new QGroupBox(centralWidget);
        registerForm->setObjectName(QString::fromUtf8("registerForm"));
        registerForm->setGeometry(QRect(240, 80, 291, 511));
        QFont font3;
        font3.setPointSize(36);
        registerForm->setFont(font3);
        registerForm->setStyleSheet(QString::fromUtf8("#registerForm{\n"
"\n"
" background: url(:/MainWindow/logbg.png) no-repeat center center fixed;\n"
"border-radius:8px;\n"
"color:white;\n"
"\n"
"}"));
        verticalLayoutWidget_2 = new QWidget(registerForm);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 50, 261, 451));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setSpacing(10);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        verticalLayout_11->setSizeConstraint(QLayout::SetFixedSize);
        label_2 = new QLabel(verticalLayoutWidget_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font2);
        label_2->setStyleSheet(QString::fromUtf8("#label_2\n"
"{\n"
"color:white;\n"
"}\n"
""));

        verticalLayout_11->addWidget(label_2);

        username = new QLineEdit(verticalLayoutWidget_2);
        username->setObjectName(QString::fromUtf8("username"));
        username->setStyleSheet(QString::fromUtf8("#username{\n"
"	background-color: rgb(56, 40, 30);\n"
"color:white;\n"
"}\n"
""));

        verticalLayout_11->addWidget(username);

        label_3 = new QLabel(verticalLayoutWidget_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font2);
        label_3->setStyleSheet(QString::fromUtf8("#label_3\n"
"{\n"
"color:white;\n"
"}\n"
""));

        verticalLayout_11->addWidget(label_3);

        password = new QLineEdit(verticalLayoutWidget_2);
        password->setObjectName(QString::fromUtf8("password"));
        password->setStyleSheet(QString::fromUtf8("#password\n"
"{\n"
"	background-color: rgb(56, 40, 30);\n"
"color:white;\n"
"}\n"
""));
        password->setEchoMode(QLineEdit::Password);

        verticalLayout_11->addWidget(password);

        label_4 = new QLabel(verticalLayoutWidget_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font2);
        label_4->setStyleSheet(QString::fromUtf8("#label_4\n"
"{\n"
"color:white;\n"
"}\n"
""));

        verticalLayout_11->addWidget(label_4);

        firstName = new QLineEdit(verticalLayoutWidget_2);
        firstName->setObjectName(QString::fromUtf8("firstName"));
        firstName->setStyleSheet(QString::fromUtf8("#firstName{\n"
"	background-color: rgb(56, 40, 30);\n"
"color:white;\n"
"}\n"
""));

        verticalLayout_11->addWidget(firstName);

        label_5 = new QLabel(verticalLayoutWidget_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font2);
        label_5->setStyleSheet(QString::fromUtf8("#label_5\n"
"{\n"
"color:white;\n"
"}\n"
""));

        verticalLayout_11->addWidget(label_5);

        lastName = new QLineEdit(verticalLayoutWidget_2);
        lastName->setObjectName(QString::fromUtf8("lastName"));
        lastName->setStyleSheet(QString::fromUtf8("#lastName{\n"
"	background-color: rgb(56, 40, 30);\n"
"color:white;\n"
"}\n"
""));

        verticalLayout_11->addWidget(lastName);

        SignUpBtn = new QPushButton(verticalLayoutWidget_2);
        SignUpBtn->setObjectName(QString::fromUtf8("SignUpBtn"));
        SignUpBtn->setFont(font2);
        SignUpBtn->setStyleSheet(QString::fromUtf8("#SignUpBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#SignUpBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout_11->addWidget(SignUpBtn);


        verticalLayout_3->addLayout(verticalLayout_11);

        MainWindowClass->setCentralWidget(centralWidget);

        retranslateUi(MainWindowClass);

        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QCoreApplication::translate("MainWindowClass", "MainWindow", nullptr));
        label->setText(QCoreApplication::translate("MainWindowClass", "REGISTER AND LOGIN FORM", nullptr));
        loginForm->setTitle(QCoreApplication::translate("MainWindowClass", "Login", nullptr));
        usernameLogin->setText(QCoreApplication::translate("MainWindowClass", "Username", nullptr));
        passwordLogin->setText(QCoreApplication::translate("MainWindowClass", "Password", nullptr));
        LogInBtn->setText(QCoreApplication::translate("MainWindowClass", "Login", nullptr));
        registerForm->setTitle(QCoreApplication::translate("MainWindowClass", "Sign up", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindowClass", "Username", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindowClass", "Password", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindowClass", "First Name", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindowClass", "Last Name", nullptr));
        SignUpBtn->setText(QCoreApplication::translate("MainWindowClass", "Sign Up", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
