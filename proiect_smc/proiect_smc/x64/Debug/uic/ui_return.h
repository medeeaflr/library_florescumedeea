/********************************************************************************
** Form generated from reading UI file 'return.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RETURN_H
#define UI_RETURN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Return
{
public:
    QWidget *widget;
    QPushButton *backBtn;

    void setupUi(QDialog *Return)
    {
        if (Return->objectName().isEmpty())
            Return->setObjectName(QString::fromUtf8("Return"));
        Return->resize(924, 434);
        widget = new QWidget(Return);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(-20, -10, 951, 451));
        widget->setStyleSheet(QString::fromUtf8("#widget{\n"
"border-image: url(:/MainWindow/mylibrary.jpg);\n"
"  color:white;\n"
"}"));
        backBtn = new QPushButton(widget);
        backBtn->setObjectName(QString::fromUtf8("backBtn"));
        backBtn->setGeometry(QRect(760, 20, 161, 24));
        backBtn->setStyleSheet(QString::fromUtf8("#backBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#backBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        retranslateUi(Return);

        QMetaObject::connectSlotsByName(Return);
    } // setupUi

    void retranslateUi(QDialog *Return)
    {
        Return->setWindowTitle(QCoreApplication::translate("Return", "Return", nullptr));
        backBtn->setText(QCoreApplication::translate("Return", "Back to Main Page", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Return: public Ui_Return {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RETURN_H
