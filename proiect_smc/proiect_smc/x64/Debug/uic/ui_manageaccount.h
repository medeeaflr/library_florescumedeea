/********************************************************************************
** Form generated from reading UI file 'manageaccount.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MANAGEACCOUNT_H
#define UI_MANAGEACCOUNT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ManageAccount
{
public:
    QWidget *widget;
    QWidget *widget_2;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *changeBtn;
    QPushButton *deleteBtn;
    QPushButton *backBtn;

    void setupUi(QDialog *ManageAccount)
    {
        if (ManageAccount->objectName().isEmpty())
            ManageAccount->setObjectName(QString::fromUtf8("ManageAccount"));
        ManageAccount->resize(876, 442);
        widget = new QWidget(ManageAccount);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(-10, -10, 891, 481));
        widget->setStyleSheet(QString::fromUtf8("#widget{\n"
"border-image: url(:/MainWindow/mylibrary.jpg);\n"
"  color:white;\n"
"}"));
        widget_2 = new QWidget(widget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setGeometry(QRect(40, 60, 821, 381));
        widget_2->setStyleSheet(QString::fromUtf8("#widget_2{\n"
"border-image: url(:/MainWindow/logbg.png);\n"
"  color:white;\n"
"}"));
        layoutWidget = new QWidget(widget_2);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(300, 100, 241, 171));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        changeBtn = new QPushButton(layoutWidget);
        changeBtn->setObjectName(QString::fromUtf8("changeBtn"));
        changeBtn->setStyleSheet(QString::fromUtf8("#changeBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#changeBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(changeBtn);

        deleteBtn = new QPushButton(layoutWidget);
        deleteBtn->setObjectName(QString::fromUtf8("deleteBtn"));
        deleteBtn->setStyleSheet(QString::fromUtf8("#deleteBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#deleteBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(deleteBtn);

        backBtn = new QPushButton(layoutWidget);
        backBtn->setObjectName(QString::fromUtf8("backBtn"));
        backBtn->setStyleSheet(QString::fromUtf8("#backBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#backBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(backBtn);


        retranslateUi(ManageAccount);

        QMetaObject::connectSlotsByName(ManageAccount);
    } // setupUi

    void retranslateUi(QDialog *ManageAccount)
    {
        ManageAccount->setWindowTitle(QCoreApplication::translate("ManageAccount", "ManageAccount", nullptr));
        changeBtn->setText(QCoreApplication::translate("ManageAccount", "Change Password", nullptr));
        deleteBtn->setText(QCoreApplication::translate("ManageAccount", "Delete account", nullptr));
        backBtn->setText(QCoreApplication::translate("ManageAccount", "Back to Main Page", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ManageAccount: public Ui_ManageAccount {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MANAGEACCOUNT_H
