/********************************************************************************
** Form generated from reading UI file 'account.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ACCOUNT_H
#define UI_ACCOUNT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Account
{
public:
    QWidget *widget;
    QWidget *m;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *borrowBtn;
    QPushButton *mybooksBtn;
    QPushButton *manageBtn;
    QPushButton *SignOutBtn;

    void setupUi(QDialog *Account)
    {
        if (Account->objectName().isEmpty())
            Account->setObjectName(QString::fromUtf8("Account"));
        Account->resize(879, 419);
        widget = new QWidget(Account);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(0, 0, 891, 421));
        widget->setStyleSheet(QString::fromUtf8("#widget{\n"
"border-image: url(:/MainWindow/mylibrary.jpg);\n"
"  color:white;\n"
"}"));
        m = new QWidget(widget);
        m->setObjectName(QString::fromUtf8("m"));
        m->setGeometry(QRect(-30, 0, 901, 431));
        m->setStyleSheet(QString::fromUtf8("#widget_2{\n"
"border-image: url(:/MainWindow/logbg.png);\n"
"  color:white;\n"
"}"));
        layoutWidget = new QWidget(m);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(340, 80, 261, 241));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        borrowBtn = new QPushButton(layoutWidget);
        borrowBtn->setObjectName(QString::fromUtf8("borrowBtn"));
        borrowBtn->setStyleSheet(QString::fromUtf8("#borrowBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#borrowBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(borrowBtn);

        mybooksBtn = new QPushButton(layoutWidget);
        mybooksBtn->setObjectName(QString::fromUtf8("mybooksBtn"));
        mybooksBtn->setStyleSheet(QString::fromUtf8("#mybooksBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#mybooksBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(mybooksBtn);

        manageBtn = new QPushButton(layoutWidget);
        manageBtn->setObjectName(QString::fromUtf8("manageBtn"));
        manageBtn->setStyleSheet(QString::fromUtf8("#manageBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#manageBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(manageBtn);

        SignOutBtn = new QPushButton(layoutWidget);
        SignOutBtn->setObjectName(QString::fromUtf8("SignOutBtn"));
        SignOutBtn->setStyleSheet(QString::fromUtf8("#SignOutBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#SignOutBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(SignOutBtn);


        retranslateUi(Account);

        QMetaObject::connectSlotsByName(Account);
    } // setupUi

    void retranslateUi(QDialog *Account)
    {
        Account->setWindowTitle(QCoreApplication::translate("Account", "Account", nullptr));
        borrowBtn->setText(QCoreApplication::translate("Account", "Borrow a Book", nullptr));
        mybooksBtn->setText(QCoreApplication::translate("Account", "See my Books", nullptr));
        manageBtn->setText(QCoreApplication::translate("Account", "Manage your account", nullptr));
        SignOutBtn->setText(QCoreApplication::translate("Account", "Sign Out", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Account: public Ui_Account {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ACCOUNT_H
