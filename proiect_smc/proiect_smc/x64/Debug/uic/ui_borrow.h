/********************************************************************************
** Form generated from reading UI file 'borrow.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BORROW_H
#define UI_BORROW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Borrow
{
public:
    QWidget *widget;
    QPushButton *backBtn;
    QWidget *widget_2;
    QTableView *tableView;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLineEdit *search;
    QPushButton *SearchTitleBtn;
    QPushButton *SearchAuthorBtn;
    QPushButton *SearchIsbnBtn;
    QPushButton *RefreshBtn;
    QPushButton *borrowBtn;

    void setupUi(QDialog *Borrow)
    {
        if (Borrow->objectName().isEmpty())
            Borrow->setObjectName(QString::fromUtf8("Borrow"));
        Borrow->resize(887, 435);
        widget = new QWidget(Borrow);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(-10, 0, 901, 451));
        widget->setStyleSheet(QString::fromUtf8("#widget{\n"
"border-image: url(:/MainWindow/mylibrary.jpg);\n"
"  color:white;\n"
"}"));
        backBtn = new QPushButton(widget);
        backBtn->setObjectName(QString::fromUtf8("backBtn"));
        backBtn->setGeometry(QRect(760, 10, 121, 24));
        backBtn->setStyleSheet(QString::fromUtf8("#backBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#backBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));
        widget_2 = new QWidget(widget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setGeometry(QRect(50, 40, 811, 371));
        widget_2->setStyleSheet(QString::fromUtf8("#widget_2{\n"
"border-image: url(:/MainWindow/page.jpg);\n"
"  color:white;\n"
"}"));
        tableView = new QTableView(widget_2);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setGeometry(QRect(0, 0, 631, 371));
        tableView->setStyleSheet(QString::fromUtf8("#tableView{\n"
"background:transparent;\n"
"  color:rgb(88, 54, 34);\n"
"}"));
        layoutWidget = new QWidget(widget_2);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(640, 140, 161, 141));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        search = new QLineEdit(layoutWidget);
        search->setObjectName(QString::fromUtf8("search"));
        search->setStyleSheet(QString::fromUtf8("#search{\n"
"	background-color: rgb(56, 40, 30);\n"
"color:white;\n"
"}\n"
""));

        verticalLayout->addWidget(search);

        SearchTitleBtn = new QPushButton(layoutWidget);
        SearchTitleBtn->setObjectName(QString::fromUtf8("SearchTitleBtn"));
        SearchTitleBtn->setStyleSheet(QString::fromUtf8("#SearchTitleBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#SearchTitleBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(SearchTitleBtn);

        SearchAuthorBtn = new QPushButton(layoutWidget);
        SearchAuthorBtn->setObjectName(QString::fromUtf8("SearchAuthorBtn"));
        SearchAuthorBtn->setStyleSheet(QString::fromUtf8("#SearchAuthorBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#SearchAuthorBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(SearchAuthorBtn);

        SearchIsbnBtn = new QPushButton(layoutWidget);
        SearchIsbnBtn->setObjectName(QString::fromUtf8("SearchIsbnBtn"));
        SearchIsbnBtn->setStyleSheet(QString::fromUtf8("#SearchIsbnBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#SearchIsbnBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(SearchIsbnBtn);

        RefreshBtn = new QPushButton(layoutWidget);
        RefreshBtn->setObjectName(QString::fromUtf8("RefreshBtn"));
        RefreshBtn->setStyleSheet(QString::fromUtf8("#RefreshBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#RefreshBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(RefreshBtn);

        borrowBtn = new QPushButton(layoutWidget);
        borrowBtn->setObjectName(QString::fromUtf8("borrowBtn"));
        borrowBtn->setStyleSheet(QString::fromUtf8("#borrowBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#borrowBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        verticalLayout->addWidget(borrowBtn);


        retranslateUi(Borrow);

        QMetaObject::connectSlotsByName(Borrow);
    } // setupUi

    void retranslateUi(QDialog *Borrow)
    {
        Borrow->setWindowTitle(QCoreApplication::translate("Borrow", "Borrow", nullptr));
        backBtn->setText(QCoreApplication::translate("Borrow", "Back to Main Page", nullptr));
        SearchTitleBtn->setText(QCoreApplication::translate("Borrow", "Search by Title", nullptr));
        SearchAuthorBtn->setText(QCoreApplication::translate("Borrow", "Search by Author", nullptr));
        SearchIsbnBtn->setText(QCoreApplication::translate("Borrow", "Search by ISBN", nullptr));
        RefreshBtn->setText(QCoreApplication::translate("Borrow", "Refresh", nullptr));
        borrowBtn->setText(QCoreApplication::translate("Borrow", "Borrow a Book", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Borrow: public Ui_Borrow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BORROW_H
