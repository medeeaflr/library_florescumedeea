/********************************************************************************
** Form generated from reading UI file 'reader.ui'
**
** Created by: Qt User Interface Compiler version 6.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_READER_H
#define UI_READER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Reader
{
public:
    QWidget *widget;
    QTextBrowser *textBrowser;
    QPushButton *returnBtn;

    void setupUi(QDialog *Reader)
    {
        if (Reader->objectName().isEmpty())
            Reader->setObjectName(QString::fromUtf8("Reader"));
        Reader->resize(857, 440);
        widget = new QWidget(Reader);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(0, 0, 861, 441));
        widget->setStyleSheet(QString::fromUtf8("#widget{\n"
"border-image: url(:/MainWindow/mylibrary.jpg);\n"
"  color:white;\n"
"}"));
        textBrowser = new QTextBrowser(widget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(60, 20, 611, 391));
        textBrowser->setStyleSheet(QString::fromUtf8("#textBrowser{\n"
"border-image: url(:/MainWindow/page.jpg);\n"
"  color:white;\n"
"}"));
        returnBtn = new QPushButton(widget);
        returnBtn->setObjectName(QString::fromUtf8("returnBtn"));
        returnBtn->setGeometry(QRect(720, 10, 121, 31));
        returnBtn->setStyleSheet(QString::fromUtf8("#returnBtn{\n"
"color:white;\n"
"background-color:rgb(88, 54, 34);\n"
"\n"
"border-width:0px;\n"
"border-radius:4px;\n"
"}\n"
"\n"
"#returnBtn:hover{\n"
"background-color:rgb(99, 60, 52);\n"
"}"));

        retranslateUi(Reader);

        QMetaObject::connectSlotsByName(Reader);
    } // setupUi

    void retranslateUi(QDialog *Reader)
    {
        Reader->setWindowTitle(QCoreApplication::translate("Reader", "Reader", nullptr));
        textBrowser->setHtml(QCoreApplication::translate("Reader", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">John Clemens, Samuel's father, was a farmer, merchant, and postmaster in a Missouri town, called Florida. His wife, Jane Clemens, was a stirring, busy woman, who liked to get her work out of the way and then have a real frolic. Her husband did not know what it meant to frolic. He was not very well to begin with, and when he had any spare time, he sat by himself figuring away on an invention, year after year. H"
                        "e spent a good deal of time, too, thinking what fine things he would do for his family when he sold a great tract of land in Tennessee. He had bought seventy-five thousand acres of land when he was much younger, for just a few cents an acre, and when that land went up in price, he expected to be pointed out as a millionaire, at least. John Clemens was a good man and something of a scholar, but he was not the least bit merry. His children never saw him laugh once in his whole life! Think of it!</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">Mrs. Clemens did not like to have any one around when she was bustling through the housework, so the six children spent the days roaming through the country, picking nuts and berries. When it came night and they had had their supper, they would crowd around th"
                        "e open fire and coax Jennie, a slave girl, or Uncle Ned, a colored farm-hand, to tell them stories.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><a name=\"aswift_3_expand\"></a><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">U</span><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">ncle Ned was a famous story-teller. When he described witches and goblins, the children would look over their shoulders as if they half expected to see the queer creatures in the room. All these stories began &quot;Once 'pon a time,&quot; but each one ended differently. One of the children, Sam Clemens, admired Uncle Ned's stories so that he could hardly wait for evening to come.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-in"
                        "dent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">Sam was a delicate child. The neighbors used to shake their heads and declare he would never live to be a man, and every one always spoke of him as &quot;little Sam.&quot;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">When Mr. Clemens moved to another town some distance away, the mother said instantly: &quot;Well, Hannibal may be all right for your business, but Florida agrees so well with little Sam, that I shall spend every summer here with the children, on the Quarles farm.&quot;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-f"
                        "amily:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">The children were glad she held to this plan, for Mr. Quarles laughed and joked with them, built them high swings, let them ride in ox-teams and go on horseback, and tumble in the hayfields all they wished. They had so much fun and exercise that they were even willing to go to bed without any stories. Sam grew plump.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">A funny thing happened the first summer they went to nice Mr. Quarles's. Mrs. Clemens, with the older children, the new baby, and Jennie, went on ahead in a large wagon. Sam was asleep. Mr. Clemens was to wait until he woke up and then was to carry him on horseback, to join the rest. Well, as Mr. Clemens was waiting for Sam to finish his nap, he go"
                        "t to thinking of his invention, or his Tennessee land, and presently he saddled and bridled the horse and rode away[184] without him. He never thought of Sam again until his wife said, as he reached the Quarles's dooryard: &quot;Where is little Sam?&quot;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><a name=\"aswift_4_expand\"></a><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">&quot;</span><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">Why\342\200\224why\342\200\224&quot; he stammered, &quot;I must have forgotten him.&quot; Of course he was ashamed of himself and hurried a man off to Hannibal, on a swift horse, where Sam was found hungry and frightened, wandering through the locked house.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-ri"
                        "ght:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">Sam was sent to school when he was five. He certainly did not like to study very well but did learn to be a fine reader and speller.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">At the age of nine, Sam was a good swimmer (although he came very near being drowned three different times, while he was learning) and loved the river so that he was to be found on its shore almost any hour of the day. He longed to travel by steamer. Once he ran away and hid on board one until it was well down the river. As soon as he showed himself to the captain, he was put ashore, his father was sent for, and he received a whipping that he re"
                        "membered a long time.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">At nine he had a head rather too large for his body, and it looked even bigger because[185] he had such a lot of waving, sandy hair. He had fine gray eyes, a slow, drawling voice, and said such droll things that the boys listened to everything he said. His two best chums were Will Bowen and John Briggs. These three friends could run like deer, and what time they were not fishing or swimming they usually spent in a cave which they had found.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">At twelve he was just a ca"
                        "reless, happy, barefoot boy, often in mischief, and only excelling in two things at school. He won the weekly medal for spelling, and his compositions were so funny that the teachers and pupils used to laugh till the tears came, when they were read aloud. His teachers said he ought to train himself for a writer, but it did not seem to him that there was anything so noble or desirable in this world as being a pilot. And he loved the great Mississippi River better than any place he had known or could imagine.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">Sam's father died, whispering: &quot;Don't sell the Tennessee land! Hold on to it, and you will all be rich!&quot;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0"
                        "px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">After his death Sam learned the printer's[186] trade. He was very quick in setting type and accurate, so that he soon helped his older brother start a newspaper. He worked with his brother until he was eighteen, and then he told his mother that he wanted to start out for himself in the world. Jane Clemens loved him dearly and hated to part with him, but when she saw his heart was set on going, she took up a testament and said: &quot;Well, Sam, you may try it, but I want you to take hold of this book and make me a promise. I want you to repeat after me these words\342\200\224'I do solemnly swear that I will not throw a card or drink a drop of liquor while I am gone!'&quot;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica"
                        "','Arial','sans-serif'; font-size:18px; color:#333333;\">He repeated these words after her, bade her good-by, and went to St. Louis. He meant to travel, and as he earned enough by newspaper work, he visited New York, Philadelphia, and was on his way to South America when he got a chance to be a pilot on the Mississippi River. While he was learning this trade, he was happier than he had ever been in his life. If you want to know what happened to him at this time you must read a book he wrote, Life on the Mississippi River. He wrote a great many books and signed whatever he wrote with a queer name\342\200\224MARK TWAIN. This was an old term used by pilots to show how deep the water is where they throw the lead. His writings, like his boyish compositions, made people laugh. So that now, although he has been dead several years, whenever the name of Mark Twain is mentioned, a smile goes around. If you want to know more about the actual doings of Sam and his chums, Will Bowen and John Briggs, read Tom Sawyer and Huc"
                        "kleberry Finn, for in those books Sam has set down a pretty fair account of their escapades.</span></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:15px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:30px;\"><span style=\" font-family:'Helvetica Neue','Helvetica','Arial','sans-serif'; font-size:18px; color:#333333;\">Mr. Clemens had a wife and children of whom he was very fond. As he made much money from his books and lectures, they were all able to travel in foreign countries, and his best book of travel is Innocents Abroad. It seems to me that even his father would have laughed over that book. Speaking of his father again reminds me to tell you that the Tennessee land never brought any luxuries to the Clemens family. It was sold for less than the taxes had amounted to.</span></p></body></html>", nullptr));
        returnBtn->setText(QCoreApplication::translate("Reader", "Back to my Books", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Reader: public Ui_Reader {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_READER_H
